/* eslint-disable @next/next/no-css-tags */
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const originalRenderPage = ctx.renderPage;

    // Run the React rendering logic synchronously
    ctx.renderPage = () =>
      originalRenderPage({
        // Useful for wrapping the whole react tree
        enhanceApp: (App) => App,
        // Useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <link
            rel="shortcut icon"
            href="assets/images/logo.png"
            type="image/png"
          />
          {/*====== FontAwesome css ======*/}
          <link
            rel="stylesheet"
            href="assets/fonts/fontawesome/css/all.min.css"
          />
          {/*====== Flaticon css ======*/}
          <link rel="stylesheet" href="assets/fonts/flaticon/flaticon.css" />
          {/*====== Bootstrap css ======*/}
          <link
            rel="stylesheet"
            href="assets/vendor/bootstrap/css/bootstrap.min.css"
          />
          {/*====== magnific-popup css ======*/}
          <link
            rel="stylesheet"
            href="assets/vendor/magnific-popup/dist/magnific-popup.css"
          />
          {/*====== Slick-popup css ======*/}
          <link rel="stylesheet" href="assets/vendor/slick/slick.css" />
          {/*====== Nice Select css ======*/}
          <link
            rel="stylesheet"
            href="assets/vendor/nice-select/css/nice-select.css"
          />
          {/*====== Animate css ======*/}
          <link rel="stylesheet" href="assets/vendor/animate.css" />
          {/*====== Default css ======*/}
          <link rel="stylesheet" href="assets/css/default.css" />
          {/*====== Style css ======*/}
          <link rel="stylesheet" href="assets/css/style.css" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
